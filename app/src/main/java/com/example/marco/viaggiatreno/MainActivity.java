package com.example.marco.viaggiatreno;



import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;

import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity{

    // Variabili Utilzzate
    public final int dim =6;
    public static final int [] IDON ={R.id.Edit1,R.id.Edit2,R.id.Edit3,R.id.Edit4,R.id.Edit5,R.id.Edit6};
    public static final int [] IDST ={R.id.Stop1,R.id.Stop2,R.id.Stop3,R.id.Stop4,R.id.Stop5,R.id.Stop};
    public static final int [] IDEY ={R.id.empty1,R.id.empty2,R.id.empty3,R.id.empty4,R.id.empty5,R.id.empty6};
    public static final int [] IDNT ={R.id.not1,R.id.not2,R.id.not3,R.id.not4,R.id.not5,R.id.not6};
    public static final int [] LAYT ={R.id.RL,R.id.RL1,R.id.RL2,R.id.RL3,R.id.RL4,R.id.RL5};
    public static final int [] LCIT ={R.id.where1,R.id.where2,R.id.where3,R.id.where4,R.id.where5,R.id.where5};
    Button [] btnOn=new Button[dim], btnSt=new Button[dim];
    static TextView [] not=new TextView[6];
    static TextView [] empty=new TextView[6];
    static TextView [] citta=new TextView[6];
    public static RelativeLayout[] ll=new RelativeLayout[6];
    String string[] = new String[6];
    int posizione[] = new int [6];
    public static int pos;
    public static String s;
    int start [] ={0,0,0,0,0,0};
    boolean pool [] ={false, false, false, false, false, false};
    Handler [] handler;
    Runnable runnable;
    int arr_aggiungi[] = {0,0,0,0,0,0};
    public static boolean [] TaskCancelled ={false, false, false, false, false, false};
    public static boolean bool;
    int stato_main[]={0,0,0,0,0,0};
    int tempoD [] = new int [6];
    SharedPreferences sharedPreferences;
    String stringat="";


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler[7];

        setContentView(R.layout.activity_main);
        sharedPreferences = getPreferences(MODE_PRIVATE);
        String t=sharedPreferences.getString("numero_t","");
        impostalay(t);



    }


    private void impostalay(String t) {

        for (int i = 0; i < dim; i++) {

            if (!t.equals("")) {
                seitreni(t);
            }
            final int tmp = i;
            btnOn[i] = findViewById(IDON[i]);
            final int finalI2 = i;
            if (!btnOn[i].getText().equals("again")) {
                neWl(i);
            }
            if (btnOn[i].getText().equals("again")) {
                String s=not[tmp].getText().toString();
                s=s.replaceAll("Treno numero : ","");
                SharedPreferences.Editor editor = sharedPreferences.edit();
                stringat +=s+ "/";
                editor.putString("numero_t", stringat);
                editor.commit();
                btnOn[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (!btnOn[finalI2].getText().equals("Stop")) {
                                if (!btnOn[finalI2].getText().equals("start")|| !btnOn[finalI2].getText().equals("stop")){

                                    final AlertDialog.Builder mBuild = new AlertDialog.Builder(MainActivity.this);
                                    final View myV = getLayoutInflater().inflate(R.layout.restart, null);
                                    mBuild.setView(myV);
                                    final AlertDialog dialog = mBuild.create();
                                    dialog.show();

                                    final EditText Tempo = myV.findViewById(R.id.Tempo_again);
                                    final Button mADD = myV.findViewById(R.id.Add);

                                    posizione[tmp] = tmp;
                                    mADD.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (!Tempo.getText().toString().equals("")) {
                                                String s = not[tmp].getText().toString();
                                                s = s.replaceAll("Treno numero : ", "");
                                                aggiungitreno(s, tmp, dialog);
                                                btnOn[finalI2].setText("Stop");
                                                tempoD[finalI2] = Integer.parseInt(Tempo.getText().toString());
                                            } else {
                                                Toast.makeText(getApplicationContext(), "Inserisci il tempo di pool!", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });

                                    final Button mExit = myV.findViewById(R.id.Exit);
                                    mExit.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                        }
                                    });
                                }else{
                                startpar(finalI2);
                                btnOn[finalI2].setText("stop");
                            }
                        }else{
                            settapar(finalI2);
                            btnOn[finalI2].setText("start");
                        }
                    }
                });

                btnSt[i]=findViewById(IDST[i]);
                final int finalI1 = i;
                btnSt[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        not[finalI1].setText(R.string.Trainnotmonitored);
                        citta[finalI1].setText(R.string.EmptySlot);
                        empty[finalI1].setText(R.string.EmptySlot);
                        btnOn[finalI1].setText("edit");
                        ll[finalI1].setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.original, null));
                        cancelMem(finalI1);
                        neWl(finalI1);
                    }
                });

            }

            not[i] = findViewById(IDNT[i]);
            citta[i] = findViewById(LCIT[i]);
            empty[i] = findViewById(IDEY[i]);

            ll[i] = findViewById(LAYT[i]);
            ll[i].setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.original, null));

        }
    }

    private void cancelMem(int finalI1) {
        String string[]=stringat.split("/");
        string[finalI1]="";
        String tmp="";
        for(int i=0;i<string.length;i++){
            tmp+=string[i]+"/";
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("numero_t", tmp);
        editor.commit();

        not[finalI1].setText(R.string.Trainnotmonitored);
        citta[finalI1].setText(R.string.EmptySlot);
        empty[finalI1].setText(R.string.EmptySlot);
        btnOn[finalI1].setText("edit");
    }

    private void neWl(final int i) {
        btnOn[i].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!btnOn[i].getText().equals("Stop")) {
                    if (not[i].getText().equals("Train not monitored")) {
                        final AlertDialog.Builder mBuild = new AlertDialog.Builder(MainActivity.this);
                        final View myV = getLayoutInflater().inflate(R.layout.aggiungi_treno, null);
                        mBuild.setView(myV);
                        final AlertDialog dialog = mBuild.create();
                        dialog.show();
                        final EditText ED = myV.findViewById(R.id.NumT);
                        final EditText Tempo = myV.findViewById(R.id.Tempo);

                        final Button mADD = myV.findViewById(R.id.Add);
                        posizione[i] = i;
                        mADD.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(!Tempo.getText().toString().equals("") && !ED.getText().toString().equals("")) {
                                    aggiungitreno(ED.getText().toString(), i, dialog);
                                    btnOn[i].setText("Stop");
                                    tempoD[i] = Integer.parseInt(Tempo.getText().toString());
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    stringat += ED.getText().toString() + "/";
                                    editor.putString("numero_t", stringat);
                                    editor.commit();
                                }else{

                                    if(Tempo.getText().toString().equals("") || ED.getText().toString().equals("") || Tempo.getText().toString().equals("") && ED.getText().toString().equals("") ){
                                        Toast.makeText(getApplicationContext(),"Inserisci i valori mancanti",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        });

                        final Button mExit = myV.findViewById(R.id.Exit);
                        mExit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                    } else {
                        startpar(i);
                        btnOn[i].setText("Stop");
                    }
                } else {
                    settapar(i);
                    btnOn[i].setText("Start");
                }
            }
        });

        btnSt[i] = findViewById(IDST[i]);
        final int finalI = i;
        btnSt[i].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!not[finalI].getText().equals("Train not monitored")) {
                    settapar(finalI);
                    cancel(finalI);
                    btnOn[finalI].setText("Edit");
                } else {
                    Toast.makeText(getApplicationContext(), "Non puoi cancellare il nulla", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void seitreni(String t) {
        String tt[]=t.split("/");
        for(int i=0; i<tt.length;i++){
            if(!tt[i].equals("")) {
                not[i] = findViewById(IDNT[i]);
                not[i].setText("Treno numero : "+tt[i]);
                citta[i] = findViewById(LCIT[i]);
                citta[i].setText("Clicca again per ripartire, altrimenti clear");
                empty[i] = findViewById(IDEY[i]);
                empty[i].setText("");
                btnOn[i] = findViewById(IDON[i]);
                btnOn[i].setText("again");
                ll[i]=findViewById(LAYT[i]);
                ll[i].setBackgroundColor(Color.CYAN);
            }
        }
    }


    private void settapar(int fin) {
        start[fin]=0;
        pool[fin]=false;
        arr_aggiungi[fin]=0;
        handler[fin].removeCallbacks(runnable);
        bool=false;
    }

    private void startpar(int tmp){
        arr_aggiungi[tmp]=1;
        start[tmp] = 1;
        string[tmp]=s;
        pos=tmp;
        TrovaTreno trenino = new TrovaTreno();
        TaskCancelled[tmp]=false;
        trenino.execute();
        handler [tmp] = new Handler();
        pool[tmp]=true;
        bool=true;
        controllotreno(tmp);
    }

    private void aggiungitreno(String ED, int tmp, AlertDialog dialog) {
        arr_aggiungi[tmp]=1;
        start[tmp] = 1;
        s= ED;
        string[tmp]=s;
        dialog.dismiss();
        pos=tmp;
        TrovaTreno trenino = new TrovaTreno();
        TaskCancelled[tmp]=false;
        trenino.execute();
        handler [tmp] = new Handler();
        pool[tmp]=true;
        bool=true;
        controllotreno(tmp);
    }

    private void cancel(int pos) {
        if(!pool[pos]) {
            TaskCancelled[pos] = true;
            not[pos].setText(R.string.Trainnotmonitored);
            empty[pos].setText(R.string.EmptySlot);
            citta[pos].setText(R.string.EmptySlot);
            ll[pos].setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.original, null));
            pool[pos]=false;
            stato_main[pos]=0;
        }
    }

    private void controllotreno(final int p) {
        runnable = new Runnable() {
            @Override
            public void run() {
                    if (start[p] == 1 && arr_aggiungi[p] == 1) {
                        s=string[p];
                        pos=p;
                        Log.d("race", "ok " + p +" "+ s);
                        TrovaTreno treno = new TrovaTreno();
                        treno.execute();
                        if (pool[p]) {
                            int tmp= tempoD[p];
                            handler[p].postDelayed(this, tmp*10000);
                            if( stato_main[p]!=TrovaTreno.stato||TrovaTreno.stato==0){
                                notification(pos);
                                stato_main[p]=TrovaTreno.stato;
                            }
                        }
                    }
            }
        };
        handler[p].postDelayed(runnable, 10000);
    }

    public void notification(int pos){
        String numerotreno =string[pos];
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(MainActivity.this);
        if(TrovaTreno.tmp.contains("ritardo")) {
            mBuilder.setSmallIcon(R.drawable.notify);
            mBuilder.setContentTitle("Treno " + numerotreno + " per " + TrovaTreno.citta);
            mBuilder.setContentText("Ritardo di " + TrovaTreno.tempo + " minuti");
        }
        if(TrovaTreno.tmp.contains("anticipo")) {
            mBuilder.setSmallIcon(R.drawable.notify);
            mBuilder.setContentTitle("Treno " + numerotreno + " per " + TrovaTreno.citta);
            mBuilder.setContentText("Anticipo di " + TrovaTreno.tempo + " minuti");
        }
        if(TrovaTreno.tmp.contains("orario")) {
            mBuilder.setSmallIcon(R.drawable.notify);
            mBuilder.setContentTitle("Treno " + numerotreno + " per " + TrovaTreno.citta);
            mBuilder.setContentText("Treno in orario");
        }
        Notification mNot;
        mNot = mBuilder.build();
        NotificationManager mNotMan = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        assert mNotMan != null;
        mNotMan.notify(1,mNot);
    }
}


package com.example.marco.viaggiatreno;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.AsyncTask;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;


class TrovaTreno extends AsyncTask<String,Void,String>{
    static String tmp;
    static int stato = 0;
    static int tempo;
    private static int color;
    static String ultimoAVV;
    static String citta;


    @Override
    protected String doInBackground(String... Void) {
        if(!MainActivity.bool){
            return tmp;
        }
        else {
            try {
                ultimoAVV="";
                URL cg = new URL("http://mobile.viaggiatreno.it/vt_pax_internet/mobile/numero?numeroTreno=" + MainActivity.s);
                BufferedReader in = new BufferedReader(new InputStreamReader(cg.openStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    if (inputLine.contains("arrivato")
                            || inputLine.contains("ritardo")
                            || inputLine.contains("partito")
                            || inputLine.contains("anticipo")
                            || inputLine.contains("orario")) {
                        tmp = inputLine;
                    }
                    else if(inputLine.contains("Ultimo rilevamento a")){
                        ultimoAVV=inputLine;
                        ultimoAVV=ultimoAVV.replaceFirst("\\s*","");
                        ultimoAVV=ultimoAVV.replaceAll("Ultimo rilevamento a","");
                        ultimoAVV=ultimoAVV.replaceAll("alle ore","");
                        ultimoAVV=ultimoAVV.replaceAll("\\d", "");
                        ultimoAVV=ultimoAVV.replaceAll(":","");
                    }
                    else if(inputLine.contains("<h2>")){
                        citta=inputLine;
                        citta= citta.replaceFirst("\\s*","");
                        citta=citta.replaceAll("[<>h2/]+","");
                    }
                }


                if (tmp.contains("anticipo")){
                    stato=1;
                    String amount = tmp.replaceAll("[^0-9]+", "");
                    tempo=Integer.parseInt(amount);
                    color=2;
                }

                if(tmp.contains("orario")){
                    stato=5;
                    color=2;
                }

                if (tmp.contains("arrivato")) {
                    stato = 2;
                    String amount = tmp.replaceAll("[^0-9]+", "");
                    tempo = Integer.parseInt(amount);
                    color = 2;
                }
                if (tmp.contains("ritardo")) {
                    stato = 3;
                    String amount = tmp.replaceAll("[^0-9]+", "");
                    tempo = Integer.parseInt(amount);
                    if (tempo > 10) {
                        color = 3;
                    } else color = 5;
                }
                if (tmp.contains("ancora partito")) {
                    stato = 4;
                    color = 4;
                    tempo = 0;
                }


                in.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return tmp;
    }

    @Override
    protected void onPostExecute(String a) {
        super.onPostExecute(tmp);
        settastringhe(MainActivity.pos,stato,tempo,color);
    }

    private void settastringhe(int p, int s, int t, int c) {
        setcolor(p,c);
        MainActivity.not[p].setText("Treno numero : " + MainActivity.s );
        MainActivity.citta[p].setText(ultimoAVV);
        int tempo_parziale = 0;

        switch (s) {
            case 1:
                if (tempo_parziale!=t){
                    tempo_parziale=t;
                    MainActivity.empty[p].setText("Anticipo di : " + tempo_parziale + " minuti");
                    break;
                }
                else{
                    break;
                }
            case 2:
                MainActivity.empty[p].setText("Treno arrivato!");
                break;
            case 3:
                if (tempo_parziale!=t){
                    tempo_parziale=t;
                    MainActivity.empty[p].setText("Ritardo di : " + tempo_parziale + " minuti");
                    break;
                }
                else{
                    break;
                }
            case 4:
                MainActivity.empty[p].setText("Treno non ancora partito");
                break;
            case 5:
                MainActivity.empty[p].setText("Trreno in orario");
        }
    }

    @SuppressLint("ResourceAsColor")
    private void setcolor(int p ,int col) {
        switch (col) {
            case 2:
                MainActivity.ll[p].setBackgroundColor(Color.GREEN);
                break;
            case 3:
                MainActivity.ll[p].setBackgroundColor(Color.RED);
                break;
            case 5:
                MainActivity.ll[p].setBackgroundColor(Color.YELLOW);
                break;
            case 4:
                MainActivity.ll[p].setBackgroundColor(Color.CYAN);
                break;
        }
    }
}

